var defaultGaugeZonesOptions = {
  lines: 12, // The number of lines to draw
  angle: 0, // The length of each line
  lineWidth: 0.44, // The line thickness
  radiusScale: 1, // Relative radius
  pointer: {
    length: 0,
    strokeWidth: 0,
    color: '#FFF' // Fill color
  },
  limitMin: 'true',   // If true, the pointer will not go past the end of the gauge
  limitMax: 'true',   // If true, the pointer will not go past the end of the gauge
  colorStart: "#444444"
};

var defaultGaugeInterval = [0, 100];
var defaultGaugePercentColors = [[0.25, "#e50909" ], [0.5, "#ffaa00" ], [0.75, "#FFDD00" ], [1.0, "#00cc06"]];

var getColor = function(value, max) {
	var percent = value / (max*1.0);
	for(var index in defaultGaugePercentColors) {
		if(percent < defaultGaugePercentColors[index][0]) {
			return defaultGaugePercentColors[index][1];
		}
	}
}

var createGauge = function(rootElement, value, textElement, interval, options) {
	var gauge = new Gauge(rootElement);
	var inter = (typeof interval == "object" && interval!=null && interval.length == 2) ? interval : defaultGaugeInterval;
	gauge.animationSpeed = 25; // set animation speed (32 is default value)
	gauge.minValue = inter[0]; // set max gauge value
	gauge.maxValue = inter[1]; // set max gauge value
	gauge.set(value); // set actual value
	defaultGaugeZonesOptions.colorStop = getColor(value, gauge.maxValue);
	gauge.setOptions(options == null ? defaultGaugeZonesOptions : options);
	if(textElement != null) {
		gauge.setTextField(textElement);
	}
}
